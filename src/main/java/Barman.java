import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Barman implements Runnable {


    boolean running = true;
    Bar bar;
    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());
    private String[] names = {"Wódka", "Wino", "Piwo", "Whiskey"};

    public Barman(Bar bar) {
        this.bar = bar;
    }

    @Override
    public synchronized void run() {
        while (running) {
            try {
                Drink drink = new Drink(Drink.getRandomDrink(names));
                System.out.println(drink.getName());
                bar.putDrink(drink);
                this.wait(5000);
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }


    }
    public void stop() {
        running = false;
    }



}
