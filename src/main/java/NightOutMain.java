import java.util.Scanner;


public class NightOutMain {

    public static void main(String[] args) {
        startTheParty();


    }

    private static void startTheParty() {
        Bar bar = new Bar();
        Barman barman = new Barman(bar);
        Client client1 = new Client("Adam", bar);
        Client client2 = new Client("Jakub", bar);
        Client client3 = new Client("Michał", bar);
        Client client4 = new Client("Antoni", bar);
        Client client5 = new Client("Lola", bar);


        Thread threadClient1 = new Thread(client1);
        Thread threadClient2 = new Thread(client2);
        Thread threadClient3 = new Thread(client3);
        Thread threadClient4 = new Thread(client4);
        Thread threadClient5 = new Thread(client5);
        Thread threadBarman = new Thread(barman);

        threadClient1.start();
        threadClient2.start();
        threadClient3.start();
        threadClient4.start();
        threadClient5.start();
        threadBarman.start();


        Scanner scanner = new Scanner(System.in);
        while (true) {
            String line = scanner.nextLine();
            if (line.equals("q")) {
                break;
            }
        }

        client1.stop();
        client2.stop();
        client3.stop();
        client4.stop();
        client5.stop();
        barman.stop();
        threadClient1.interrupt();
        threadClient2.interrupt();
        threadClient3.interrupt();
        threadClient4.interrupt();
        threadClient5.interrupt();
        threadBarman.interrupt();








    }
}
