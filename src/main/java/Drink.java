import java.util.Arrays;
import java.util.Random;

public class Drink {

    private String[] names = {"Wódka", "Wino", "Piwo", "Whiskey"};
    private String name;


    public Drink(String name) {
        this.name = name;

    }

    public static String getRandomDrink(String[] names) {
        int ranDrinkIndex = new Random().nextInt(names.length);
        return names[ranDrinkIndex];
    }

    public String prepareDrink() {
        return name;
    }
    @Override
    public String toString() {
        return prepareDrink();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
