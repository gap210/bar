import java.util.logging.Logger;

public class Bar {



    private Drink drink = null; //creating a 'spot' for a drink
    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());




    public synchronized void putDrink (Drink drink) { //method puts a drink on a table an noifies all interested
        this.drink = drink;
        notifyAll();

    }

    public synchronized Drink takeDrink() throws InterruptedException { //method take a drink if it is available

        while (drink == null) {  //until there is no drink available, the metod will wait outside the critical section
            this.wait();
        }
        Drink dri = drink; // creating a local variable to represent a drink that has just been put on the table
         // setting the value of the just taken drink to null
        drink = null;
        return dri;


    }
    public synchronized void nullifyDrink() {
        drink = null;
        notifyAll();

    }


}
