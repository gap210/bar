import java.util.logging.Level;
import java.util.logging.Logger;

public class Client implements Runnable{

    private String name;
    private static final Logger log = Logger.getLogger(Client.class.getCanonicalName());
    private Bar bar;
    private boolean running = true;

    public Client(String name, Bar bar) {
        this.name = name;
        this.bar = bar;
    }

    @Override
    public synchronized void run(){
        while (running) {
            try {
                bar.takeDrink();
                System.out.println(name + " drinking " + (bar.takeDrink()).toString());
                this.wait(bar.takeDrink().toString().length()*1000);

            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
    public void stop() {
        running = false;
    }
}
